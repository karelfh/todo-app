import React, { Component } from "react";

export class TodoForm extends Component {
  state = {
    task: "",
    time: ""
  };

  // Update state with what user typed in to the input
  // with the same name (e.g. name="task", state: task)
  onInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onFormSubmit = e => {
    e.preventDefault();

    // Call function from props with task and time that user typed in
    this.props.addTodo(this.state);
    // Reset state so that input is cleared when submitted
    this.setState({
      task: "",
      time: ""
    });
  };

  render() {
    return (
      <section className="form__container">
        <form className="todo__form" onSubmit={this.onFormSubmit}>
          <div className="form__item">
            <label className="form__label" htmlFor="task">
              <span className="form__label--required">* </span>Task:
            </label>
            <input
              className="form__input"
              id="task"
              placeholder="Your task is..."
              type="text"
              name="task"
              value={this.state.task}
              onChange={this.onInputChange}
              required
            />
          </div>
          <div className="form__item">
            <label className="form__label" htmlFor="time">
              Reminder:
            </label>
            <input
              className="form__input"
              id="time"
              type="time"
              name="time"
              value={this.state.time}
              onChange={this.onInputChange}
            />
          </div>
          <button className="form__button" type="submit">
            Add ToDo
          </button>
        </form>
      </section>
    );
  }
}

export default TodoForm;
