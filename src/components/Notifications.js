import React, { Component } from "react";

class Notifications extends Component {
  componentDidMount() {
    this.checkTime();
    // Set interval to run function every 60sec to check time
    this.interval = setInterval(() => this.checkTime(), 60000);
  }

  componentWillUnmount() {
    // Reset interval to 0
    clearInterval(this.interval);
  }

  createNotification = todo => {
    let text = `HEY! Your todo '${todo.task}' is now overdue.`;

    return new Notification(`ToDo App - Reminder ${todo.time}`, {
      body: text
    });
  };

  // Check if current time is the same as time set in todo
  checkTime = () => {
    let now = new Date();
    let hours = now.getHours();
    let minutes = now.getMinutes();
    let currentTime = "";

    // Unify time format (hh:mm) for current time and todo time
    minutes <= 9
      ? (currentTime = `${hours}:0${minutes}`)
      : (currentTime = `${hours}:${minutes}`);

    this.props.todos.forEach(todo => {
      return currentTime === todo.time
        ? this.createNotification(todo) 
        : null;
    });

    console.log("Check", currentTime);
  };

  render() {
    return <React.Fragment />;
  }
}

export default Notifications;
