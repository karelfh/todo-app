import React from "react";

const TodoList = ({ todos, onClick }) => {
  return (
    <div className="todo__list">
      {todos.length ? (
        todos.map(todo => {
          return (
            <div className="todo__item" key={todo.id}>
              <div className="todo__content">
                <span className="todo__task">{todo.task}</span>           
                <span className="todo__time">{todo.time}</span>
              </div>
              <div
                className="todo__check"
                id="check"
                onClick={() => onClick(todo.id)}
              />
              
            </div>
          );
        })
      ) : (
        <p className="todo__notice">So empty</p>
      )}
    </div>
  );
};

export default TodoList;
