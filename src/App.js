import React, { Component } from "react";

import Layout from "./layout/Layout";
import Notifications from "./components/Notifications";
import TodoList from "./components/TodoList";
import TodoForm from "./components/TodoForm";

class App extends Component {
  state = {
    todos: [],
    finishedTodos: [],
    notificationAccess: false
  };

  componentDidMount() {
    this.getInitialStateFromStorage();
    this.checkNotification();
  }

  // Get data from local storage
  getInitialStateFromStorage = () => {
    // For all items in state
    for (let key in this.state) {
      // If the key exists in local storage
      if (localStorage.hasOwnProperty(key)) {
        // Get the key's value from local storage
        let value = localStorage.getItem(key);

        // Parse the local storage string and set state
        value = JSON.parse(value);
        this.setState({ [key]: value });
      }
    }
  };

  checkNotification = () => {
    Notification.requestPermission()
      .then(permission => {
        if (permission !== "granted" && permission !== "default") {
          alert(
            "Unable to show notification for your tasks. Please enable notification alerts to see them."
          );
          throw new Error("Notification alerts denied by user.");
        }
      })
      .then(() => {
        this.setState({ notificationAccess: true });
      })
      .catch(err => {
        console.error(err);
      });
  };

  addTodo = todo => {
    // Generate random todo id (<0, 1>)
    // not perfect, small chance to have duplicate
    todo.id = Math.random();

    // Copy state and add the new todo to it
    let todos = [...this.state.todos, todo];
    this.setState({ todos });

    // Set new added todo to local storage
    localStorage.setItem("todos", JSON.stringify(todos));
  };

  // Remove todo completely from finished todos
  removeTodo = id => {
    const { finishedTodos } = this.state;
    // Remove clicked todo from finished todos
    const newFinishedTodos = finishedTodos.filter(todo => todo.id !== id);

    this.setState({ finishedTodos: newFinishedTodos });

    // Remove todo from local storage
    localStorage.setItem("finishedTodos", JSON.stringify(newFinishedTodos));
  };

  onTodoFinish = id => {
    const { todos, finishedTodos } = this.state;
    // Find what todo is being clicked on
    const clickedTodo = todos.find(todo => todo.id === id);
    // Find index of todo that is being clicked
    const index = todos.indexOf(clickedTodo);

    // Copy the cliked todo to finished todos state
    let finishedTodo = [...finishedTodos, clickedTodo];

    // Copy the array without clicked todo
    const newTodos = [
      // Copy array items to the index item (clicked todo)
      ...todos.slice(0, index),
      // Copy array items after the index item (clicked todo)
      ...todos.slice(index + 1, todos.length)
    ];

    this.setState({
      todos: newTodos,
      finishedTodos: finishedTodo
    });

    // Move todos in local storage to  different key
    localStorage.setItem("todos", JSON.stringify(newTodos));
    localStorage.setItem("finishedTodos", JSON.stringify(finishedTodo));
  };

  render() {
    const { todos, finishedTodos, notificationAccess } = this.state;

    let notifications = notificationAccess ? (
      <Notifications todos={todos} />
    ) : null;

    return (
      <React.Fragment>
        {notifications}

        <Layout>
          <TodoForm addTodo={this.addTodo} />

          <section className="todo__lists">
            <div className="list__item">
              <h2 className="list__heading">ToDos to finish</h2>
              <TodoList todos={todos} onClick={this.onTodoFinish} />
            </div>

            <div className="list__item">
              <h2 className="list__heading">Finished ToDos</h2>
              <TodoList todos={finishedTodos} onClick={this.removeTodo} />
            </div>
          </section>
        </Layout>
      </React.Fragment>
    );
  }
}

export default App;
