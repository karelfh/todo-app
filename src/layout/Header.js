import React from "react";

const Header = () => {
  return (
    <header className="header">
      <h1 className="title">ToDo App</h1>
    </header>
  );
};

export default Header;
